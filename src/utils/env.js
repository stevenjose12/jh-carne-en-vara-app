let data = {};

const mode = {
  kelvin: 0,
  scorpion: 1,
  yisus: 2,
  enorelis: 3,
  mantisjr: 4,
  steven: 5,
  jonathan: 6,
  production: 7,
};

let Base;

switch (mode.production) {
  case mode.enorelis:
    Base = '192.168.1.110';
    data = {
      BASE_API: 'http://' + Base + '/carne/public/api/',
      BASE_URL: 'http://' + Base + '/carne/public/',
    };
    break;
  case mode.kelvin: // Local
    Base = '192.168.1.125';
    data = {
      BASE_API: 'http://' + Base + '/CarneEnVara/api/public/api/',
      BASE_URL: 'http://' + Base + '/CarneEnVara/api/public/',
    };
    break;
  case mode.scorpion: // Scorpion
    Base = '192.168.1.5';
    data = {
      BASE_API: 'http://' + Base + '/CarneEnVara/public/api/',
      BASE_URL: 'http://' + Base + '/CarneEnVara/public/',
    };
    break;
  case mode.yisus:
    Base = '192.168.1.133';
    data = {
      BASE_API: 'http://' + Base + '/carne/public/api/',
      BASE_URL: 'http://' + Base + '/carne/public/',
    };
    break;
  case mode.mantisjr:
    Base = '159.89.92.128';
    data = {
      BASE_API: 'http://' + Base + '/CarneEnVara/api/public/api/',
      BASE_URL: 'http://' + Base + '/CarneEnVara/api/public/',
    };
    break;
  case mode.steven:
    Base = '192.168.1.116';
    data = {
      BASE_API: 'http://' + Base + '/CarneEnVara/api/public/api/',
      BASE_URL: 'http://' + Base + '/CarneEnVara/api/public/',
    };
    break;
  case mode.jonathan:
    Base = 'http://192.168.1.123/jh-carne-en-vara-api/';
    data = {
      BASE_API: Base + 'public/api/',
      BASE_URL: Base + 'public/',
    };
    break;
  case mode.production:
    Base = 'https://publicserverxyz.xyz/CarneEnVara/api/';
    data = {
      BASE_API: Base + 'public/api/',
      BASE_URL: Base + 'public/',
    };
    break;
}

// Datos de redes sociales
const social = {
  _google: {
    client_id:
      '577066695214-bmhol9a0lb8eibs5pvje41juqv93qb3m.apps.googleusercontent.com',
    reversed_client_id:
      'com.googleusercontent.apps.577066695214-bmhol9a0lb8eibs5pvje41juqv93qb3m',
  },
  _facebook: {
    app_id: '520227088575026',
    secret_id: '7ab67ace5ce53090b7d7faec4658bd74',
  },
};

export default {
  ...data,
  ...social,
};
