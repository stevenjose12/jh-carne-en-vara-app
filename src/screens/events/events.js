import PhotoView from '@merryjs/photo-viewer';
import React from 'react';
import {
  Dimensions,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableWithoutFeedback as RNTouchableWithoutFeedback,
  View,
  Platform,
} from 'react-native';
import Strings from '../../assets/lang';
import noEvents from '../../assets/icons/event_list.png';
import {Alert, axios, Colors, ENV as Environment} from '../../utils';
import {LazyImage} from '../../widgets';
import {TouchableWithoutFeedback as RNGHTouchableWithoutFeedback} from 'react-native-gesture-handler';

const Touchable =
  Platform.OS === 'ios'
    ? RNGHTouchableWithoutFeedback
    : RNTouchableWithoutFeedback;

class Events extends React.Component {
  state = {
    visible: false,
    events: [],
  };

  componentDidMount() {
    this.props.navigation.setParams({
      iconColor: Colors.red,
    });

    axios
      .post('events/get')
      .then(res => {
        if (res.data.result) {
          this.setState({
            events: res.data.events,
          });
        }
      })
      .catch(err => {
        console.log(err);
        Alert.showError();
      });
  }

  render() {
    return (
      <React.Fragment>
        <PhotoView
          visible={this.state.visible}
          data={this.state.events.map(item => {
            return {
              source: {uri: Environment.BASE_URL + item.file},
            };
          })}
          hideShareButton={true}
          hideStatusBar={Platform.OS !== 'ios'}
          onDismiss={() => {
            this.setState({visible: false});
          }}
        />
        {this.state.events.length ? (
          <ScrollView>
            <View>
              {this.state.events.map((item, index) => (
                <Touchable
                  onPress={() => {
                    this.setState({
                      visible: true,
                    });
                  }}>
                  <LazyImage
                    key={index}
                    style={styles.image}
                    source={{uri: Environment.BASE_URL + item.file}}
                  />
                </Touchable>
              ))}
            </View>
          </ScrollView>
        ) : (
          <View style={styles.noEventsContainer}>
            <View style={styles.noEventsImageContainer}>
              <Image
                style={styles.noEventsImage}
                source={noEvents}
                resizeMod="cover"
                tintColor={Colors.primary}
              />
            </View>
            <Text style={styles.noEventsText}>{Strings.notEvents}</Text>
          </View>
        )}
      </React.Fragment>
    );
  }
}

const styles = StyleSheet.create({
  image: {
    height: Dimensions.get('window').height,
    width: Dimensions.get('window').width,
  },
  noEventsContainer: {
    alignItems: 'center',
    height: '100%',
    justifyContent: 'center',
    width: '100%',
  },
  noEventsImageContainer: {
    width: 124,
    height: 124,
  },
  noEventsImage: {
    width: '100%',
    height: '100%',
  },
  noEventsText: {
    color: Colors.primary,
    fontSize: 16,
    fontWeight: 'bold',
    marginVertical: 40,
  },
});

export default Events;
